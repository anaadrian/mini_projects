import random
import time
import os


def game():
    computer_choices = ['rock', 'paper', 'scissors']
    choices = {'rock': 0, 'paper': 1, 'scissors': 2}

    computer = random.choice(computer_choices)
    player = input('Select "rock", "paper" or "scissors": ').lower()
    print()
    computer_val = choices[computer]
    player_val = choices.get(player)

    while player_val is None:
        player = input('I don\'t know what that is, select "rock", "paper" or "scissors": ').lower()
        player_val = choices.get(player)

    print()
    hand = f'You are {player} and computer is {computer}'

    dif = abs(player_val - computer_val)

    if dif == 0:
        return f'{hand}\nTIE'

    if dif == 2:
        if player_val > computer_val: return f'{hand}\nComputer wins!'
        else: return f'{hand}\nYou win!'

    if dif == 1:
        if player_val > computer_val: return f'{hand}\nYou win!'
        else: return f'{hand}\nComputer wins!'

playing = True
human_score = 0
computer_score = 0

while playing:
    result = game()

    if 'Computer' in result: computer_score += 1
    elif 'You' in result and 'TIE' not in result: human_score += 1

    print(result)
    print()
    playing = input('Do you want to play again? "Y/n": ').upper()
    print()

    os.system('cls')

    if playing not in ['Y', 'YES']:
        playing = False
        print(f'-----SCORE------\n\nYou won {human_score} times\nComputer won {computer_score} times')
        print('\nBYE')
        time.sleep(3)